$( document ).ready(function() {
    console.log( "ready!" );

    var dateObj = new Object;
    var counter = 0;

    //set the Date object based on the data from the POST request
    function setDateObject(data){
        dateObj = data;
    }

    function powerFrontEnd(dateObj){
        $("#countdown-to-year").html(dateObj.year);
        $("#countdown-to-month").html(fixZero(dateObj.month));
        $("#countdown-to-day").html(fixZero(dateObj.day));
        $("#countdown-to-hour").html(fixZero(dateObj.hour));
        $("#countdown-to-minute").html(fixZero(dateObj.minute));
        $("#countdown-to-second").html(fixZero(dateObj.second));
    }

    function fixZero(obj){
        return ('0'  + obj).slice(-2);
    }

    function countdownLoop(){
        
        var x = setInterval(function(){ 

            var timestampDate = new Date(dateObj.timestamp * 1000);
            var countDownTimestamp = timestampDate.getTime();

             // Get todays date and time
            var currentTimestamp = new Date().getTime();

            // Correction, this equals 2 hours, timestamp is somehow behind
            var correction = 2*60*60*1000;

            // Find the distance between now an the count down date
            var distance = countDownTimestamp - (currentTimestamp + correction);

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

            // If the count down is finished...
            if (distance < 0) {
                document.getElementById("countdown").innerHTML = "EXPIRED";
                window.location.href = dateObj.url;
            }

        }, 1000);
    }

    $.post( "scripts/api.php", function( data ) {
        setDateObject(data);
        powerFrontEnd(dateObj);
    });

    countdownLoop();

});