<?php 
    include('Countdown.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <?php 
                // $carbon = new Countdown();
                // echo var_dump($carbon->getDate());
            ?>
            <h1>Countdown naar:</h1>
            <div id="countdown-from">
                <span id="countdown-to-day"></span>
                <span id="countdown-to-month"></span>
                <span id="countdown-to-year"></span>
                <span id="countdown-to-hour"></span>
                <span id="countdown-to-minute"></span>
                <span id="countdown-to-second"></span>
            </div>

            <p id="countdown"></p>


        </div>



    </body>

    <script
	    src="https://code.jquery.com/jquery-3.2.1.min.js"
		integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">
    </script>
    <script src="countdown-script.js"></script>
</html>