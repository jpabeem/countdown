<?php

//namespace Scripts;

require 'Carbon/Carbon.php';

use Carbon\Carbon;

class Countdown
{
    /**
     * The countdown constants.
     */
    const DAY = 14;
    const MONTH = 8;
    const YEAR = 2017;
    const HOUR = 15;
    const MINUTE = 24;
    const SECOND = 00;
    const URL = 'https://www.tweakers.net';

    /**
     * The constructed date, accessible by API or through the getter.
     */
    protected $date;

    public function __construct()
    {
        $this->date = Carbon::create(Countdown::YEAR, Countdown::MONTH, Countdown::DAY,
                        Countdown::HOUR, Countdown::MINUTE, Countdown::SECOND);
    }

    protected function setupDateArray()
    {
        $date = [
            'year' => $this->date->year,
            'month' => $this->date->month,
            'day' => $this->date->day,
            'hour' => $this->date->hour,
            'minute' => $this->date->minute,
            'second' => $this->date->second,
            'timestamp' => $this->date->timestamp,
            'url' => Countdown::URL
        ];
        return $date;
    }

    public function getDate()
    {
        $date = $this->setupDateArray();

        return $date;
    }

}

?>