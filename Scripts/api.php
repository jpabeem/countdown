<?php 

    include('../Countdown.php');

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
   

    //check for the request method
    if ($method == 'POST') {
        $countdown = new Countdown();
        header('Content-Type: application/json');
        echo json_encode($countdown->getDate());
    }else{
        header("HTTP/1.1 400 Bad Request");
    }

?>